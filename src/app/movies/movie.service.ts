import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {MovieListRepsonse} from './class/movie-list-response';
import {Movie} from './class/movie';
import {Genre} from './class/genre';

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  tmbdUrl: string;
  tmdbKey: string;
  headers: HttpHeaders;

  constructor(private http: HttpClient) {
    this.tmdbKey = '835b003bc1531c65812e26505759cdf3';
    this.tmbdUrl = 'https://api.themoviedb.org/3/';
    this.headers = new HttpHeaders().set('Content-Type', 'application/json;charset=utf-8');
  }

  public getMovies() {
    const params = new HttpParams().set('api_key', this.tmdbKey);
    return this.http.get<MovieListRepsonse>(this.tmbdUrl + 'movie/popular', {headers: this.headers, params: params});
  }

  public getMovieDetails(id: string) {
    const params = new HttpParams().set('api_key', this.tmdbKey);
    return this.http.get<Movie>(this.tmbdUrl + 'movie/' + id, {
      headers: this.headers,
      params: params.set('append_to_response', 'credits')
    });
  }

  public searchMovies(q: string) {
    const params = new HttpParams().set('api_key', this.tmdbKey).set('query', encodeURI(q));
    return this.http.get<MovieListRepsonse>(this.tmbdUrl + 'search/movie', {
      headers: this.headers,
      params
    });
  }

  public discoverMovies(year?: string, genres?: number[]) {
    let params = new HttpParams().set('api_key', this.tmdbKey);
    if (year) {
      params = params.set('primary_release_year', year);
    }
    if (genres) {
      params = params.set('with_genres', genres.join(','));
    }
    return this.http.get<MovieListRepsonse>(this.tmbdUrl + 'discover/movie', {headers: this.headers, params});
  }

  public getGenres() {
    const params = new HttpParams().set('api_key', this.tmdbKey);
    return this.http.get<{ genres: Genre[] }>(this.tmbdUrl + 'genre/movie/list', {headers: this.headers, params});
  }
}
