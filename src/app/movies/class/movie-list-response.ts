import {Movie} from './movie';

/**
 * Class returned by TMDb API
 */
export class MovieListRepsonse {
  page: number;
  totalResults: number;
  totalPages: number;
  results: Movie[];
}
