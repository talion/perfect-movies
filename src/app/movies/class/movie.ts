import { Genre } from './genre';

export class Movie {
  id: number;
  title: string;
  poster_path: string;
  release_date: string;
  overview: string;
  runtime: number;
  duration: string;
  backdrop_path: string;
  vote_average: number;
  genres: Genre[];
  credits: {
    cast: {
      character: string;
      name: string;
    }[],
    crew: {
      job: string;
      name: string;
    }[]
  };
}
