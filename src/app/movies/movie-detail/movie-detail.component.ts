import {Component, OnInit} from '@angular/core';
import {MovieService} from '../movie.service';
import {Movie} from '../class/movie';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {MovieListRepsonse} from '../class/movie-list-response';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss'],
  providers: [MovieService]
})
export class MovieDetailComponent implements OnInit {
  movie$: Movie;

  constructor(
    private route: ActivatedRoute,
    private movieService: MovieService
  ) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.movieService.getMovieDetails(params.get('id')).subscribe((data: Movie) => {
        this.movie$ = {...data};
        this.movie$.duration = this.time_convert(this.movie$.runtime);
      });
    });
  }

  /**
   * Convert minutes to hours-minutes string
   * @param num Number of minutes to convert to string '%h %mn'
   */
  private time_convert(num: number) {
    const hours = Math.floor(num / 60);
    const minutes = num % 60;
    return `${hours}h ${minutes}mn`;
  }

}
