import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MoviesRoutingModule } from './movies-routing.module';
import {MovieDetailComponent} from './movie-detail/movie-detail.component';
import {MovieListComponent} from './movie-list/movie-list.component';
import { MovieTileComponent } from './movie-tile/movie-tile.component';
import {HttpClientModule} from '@angular/common/http';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from 'ngx-bootstrap/tooltip';


@NgModule({
  declarations: [
    MovieDetailComponent,
    MovieListComponent,
    MovieTileComponent
  ],
  imports: [
    CommonModule,
    MoviesRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    NgSelectModule,
    FormsModule,
    TooltipModule.forRoot()
  ]
})
export class MoviesModule { }
