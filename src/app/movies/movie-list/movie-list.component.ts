import {Component, OnInit} from '@angular/core';
import {MovieService} from '../movie.service';
import {Movie} from '../class/movie';
import {MovieListRepsonse} from '../class/movie-list-response';
import {faSearch, IconDefinition} from '@fortawesome/free-solid-svg-icons';
import {Genre} from '../class/genre';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
  providers: [MovieService]
})
export class MovieListComponent implements OnInit {
  movies: Movie[];
  faSearch: IconDefinition;
  genres: Genre[];
  years: string[];
  yearSelected: string;
  genresSelected: number[];

  constructor(private movieService: MovieService) {
    this.faSearch = faSearch;
    this.yearSelected = null;
    this.genresSelected = null;
    movieService.getMovies().subscribe((data: MovieListRepsonse) => {
      this.movies = [...data.results.slice(0, 12)];
    });
    movieService.getGenres().subscribe((data: { genres: Genre[] }) => {
      this.genres = [...data.genres];
    });
    this.years = [];
    for (let i = new Date().getFullYear(); i >= 1900; i--) {
      this.years.push(i.toString());
    }
  }

  onQueryKeyUp(q: string) {
    // Search for movies if there is a query string, else show populars
    // Only show the first 12 results
    this.genresSelected = null;
    this.yearSelected = null;
    if (q.length > 0) {
      this.movieService.searchMovies(q).subscribe((data: MovieListRepsonse) => {
        this.movies = [...data.results.slice(0, 12)];
      });
    } else {
      this.movieService.getMovies().subscribe((data: MovieListRepsonse) => {
        this.movies = [...data.results.slice(0, 12)];
      });
    }
  }

  onFiltersChange() {
    if (this.yearSelected || this.genresSelected) {
      this.movieService.discoverMovies(this.yearSelected, this.genresSelected).subscribe((data: MovieListRepsonse) => {
        this.movies = [...data.results.slice(0, 12)];
      });
    } else {
      this.movieService.getMovies().subscribe((data: MovieListRepsonse) => {
        this.movies = [...data.results.slice(0, 12)];
      });
    }
  }

  ngOnInit() {
  }

}
