import {Component, Input, OnInit} from '@angular/core';
import {Movie} from '../class/movie';
import {faPlus, faCheck, faEye, IconDefinition} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-movie-tile',
  templateUrl: './movie-tile.component.html',
  styleUrls: ['./movie-tile.component.scss']
})
export class MovieTileComponent implements OnInit {
  faPlus: IconDefinition;
  faCheck: IconDefinition;
  faEye: IconDefinition;
  moviesStored: number[];
  moviesSeen: number[];
  stored: boolean;
  seen: boolean;

  @Input() movie: Movie;

  constructor() {
    this.faPlus = faPlus;
    this.faCheck = faCheck;
    this.faEye = faEye;
    this.moviesStored = JSON.parse(localStorage.getItem('moviesStored'));
    this.moviesSeen = JSON.parse(localStorage.getItem('moviesSeen'));
    if (!this.moviesStored) {
      this.moviesStored = [];
    }
    if (!this.moviesSeen) {
      this.moviesSeen = [];
    }
  }

  ngOnInit() {
    this.stored = this.moviesStored.findIndex((m) => this.movie.id === m) > 0;
    this.seen = this.moviesSeen.findIndex((m) => this.movie.id === m) > 0;
  }

  addToMoviesStored(id: number) {
    this.stored = true;
    this.moviesStored = JSON.parse(localStorage.getItem('moviesStored'));
    if (!this.moviesStored) {
      this.moviesStored = [];
    }
    this.moviesStored.push(id);
    localStorage.setItem('moviesStored', JSON.stringify(this.moviesStored));
  }

  addToMoviesSeen(id: number) {
    this.seen = true;
    this.moviesSeen = JSON.parse(localStorage.getItem('moviesSeen'));
    if (!this.moviesSeen) {
      this.moviesSeen = [];
    }
    this.moviesSeen.push(id);
    localStorage.setItem('moviesSeen', JSON.stringify(this.moviesSeen));
  }

}
